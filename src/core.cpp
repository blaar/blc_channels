#include "core.hpp"

#include "blc_tools.h"
#include "blc_text.h"
#include <errno.h>
#include <sys/wait.h>

blc_channel *channels=NULL;
int channels_nb=0;

blc_process *processes=NULL;
int processes_nb=0;

/*
blc_process *blc_process_find_with_channel(blc_process *processes, int processes_nb, blc_channel *channel){
    blc_process *process;
    int position;

    FOR_EACH_INV(process, processes, processes_nb){
        position=GET_ITEM_POSITION(process->bidirectional_channels, process->bidirectional_channels_nb, &channel);
        if (position==-1) position=GET_ITEM_POSITION(process->input_channels, process->input_channels_nb, &channel);
        if (position==-1) position=GET_ITEM_POSITION(process->output_channels, process->output_channels_nb, &channel);
        if (position!=-1) return process;
    }
    return NULL;
}*/
/*
int blc_process_find_all_with_channel(blc_process ***found_processes, blc_process *processes, int processes_nb, blc_channel *channel){
    blc_process *process;
    int position;
    int found_processes_nb=0;
    
    FOR_EACH_INV(process, processes, processes_nb){
       position=GET_ITEM_POSITION(process->bidirectional_channels, process->bidirectional_channels_nb, &channel);
        if (position==-1) position=GET_ITEM_POSITION(process->input_channels, process->input_channels_nb, &channel);
        if (position==-1) position=GET_ITEM_POSITION(process->output_channels, process->output_channels_nb, &channel);
        if (position!=-1) {
            APPEND_ITEM(found_processes, &found_processes_nb, &process);
        }
    }
    return found_processes_nb;
}*/

/**
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int get_channel_index_by_name_or_id(char const *name)
{
    int i, id;
    char *end;
    
    if (name[0] == '/'){
        FOR(i, channels_nb) if (strcmp(channels[i].name, name) == 0) return i;
    }
    else {
        id = strtol(name, &end, 10);
        if (end != name && end[0] == 0){
            FOR(i, channels_nb)   if (channels[i].id == id) return i;
        }
        else color_fprintf(BLC_RED, stderr, "You have to give either an id or a channel name stating by '/' instead of '%s'.\n", name);
    }
    return -1;
}


void blc_update_shared_files(blc_process **processes, int *processes_nb, blc_channel *channels, int channels_nb)
{
    blc_mem mem;
    char buf[LINE_MAX];
    char *path, *str;
    int stdout_pipe[2];
    pid_t pid;
    char letter, access;
    ssize_t n;
    int status, pos, len;
    blc_process *process=NULL, tmp_process;
    blc_channel tmp_info;
    blc_channel *channel;
    
    SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
    pid=fork();
    if (pid==0)
    {
        //SPRINTF(buf, "-p^%d", blaar_pid); Exclude itself
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
        blc_close_pipe(stdout_pipe);
        
        //0-999 means only physical file (no socket etc )
        SYSTEM_ERROR_CHECK(execlp("lsof", "lsof", "-a", "-wlnP", "-F", "actn", "-d", "0-999", NULL), -1, NULL);
    }
    else
    {
        mem.chars=NULL;
        close(stdout_pipe[1]);
        //     destroy_file_infos(&files, &files_nb);
        do{
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=read(stdout_pipe[0], buf, sizeof(buf)), -1, EINTR, "read lsof stdout");
            mem.append(buf, n);
        }while(n!=0);
        waitpid(pid, &status, 0);
        close(stdout_pipe[0]);
        
        mem.append("", 1); //end of string
        
        str=mem.chars;
        do{
            letter=str[0];
            pos=0;
            
            str++;
            switch (letter){
                case 'p':
                    CLEAR(tmp_process);
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%d\n%n", &tmp_process.pid, &pos), 1, NULL);
                    process=NULL;
                    break;
                case 'c':
                    SYSTEM_SUCCESS_CHECK(sscanf(str, "%"STRINGIFY_CONTENT(NAME_MAX)"s\n", tmp_process.command), 1, NULL);
                    str=strchr(str, '\n')+1;
                    break;
                case 'a':
                    access=str[0];
                    str+=3; //[acces]\nt
                    len=0;
#ifdef __OpenBSD__
                    if (strncmp(str, "PSXSHM\nn", strlen("PSXSHM\nn"))==0) len=strlen("PSXSHM\nn"); //on OSX it is <channel_name with />
#else
                    if (strncmp(str, "REG\nn/dev/shm", strlen("REG\nn/dev/shm"))==0) len=strlen("REG\nn/dev/shm");  //On Linux it is /dev/shm<channel_name with />
#endif
                    if(len) {
                        str+=len;
                        path=str;
                        str=strchr(str, '\n');
                        *str=0;
                        FOR_EACH_INV(channel, channels, channels_nb){
                            if (strcmp(channel->name, path)==0){
                                FOR_EACH_INV(process, *processes, *processes_nb) if (process->pid == tmp_process.pid) break;
                                if(process==*processes-1){//Process not found
                                    process=(blc_process*)APPEND_ITEM(processes, processes_nb, &tmp_process);
                                    blc_processes_refresh_files(process, 1);
                                }
                                break;
                            }
                        }
                        str++;
                        
                        
                        //We have not found channel
                        /*   if (channel != channels-1){
                         if (process==NULL) // It is a new process session.
                         {
                         process=blc_find_process(tmp_process.pid);
                         if (process==NULL) APPEND_ITEM(&blc_processes, &blc_processes_nb, &tmp_process);
                         }
                         
                         file=blc_find_file(path);
                         if (file==NULL)//the file has never been referenced
                         {
                         tmp_file.openings=NULL;
                         tmp_file.openings_nb=0;
                         tmp_file.channel=channel;
                         file=(blc_file*)APPEND_ITEM(&files, &files_nb, &tmp_file);
                         }
                         tmp_file_opening.process=process;
                         APPEND_ITEM(&file->openings, &file->openings_nb, &tmp_file_opening);
                         }*/
                    }
                    else  str=strchr(str, '\n')+1; //We pass the non shared memory file
                    break;
                default:
                    str=strchr(str, '\n')+1;
                    break;
            }
            
            str+=pos;
        }while(str[0]!=0);
        mem.allocate(0);
    }
}




