#ifndef CORE_HPP
#define CORE_HPP

#include "blc_channel.h"
#include "blc_process.h"


extern blc_channel *channels;
extern int channels_nb;

extern blc_process *processes;
extern int processes_nb;


//blc_process *blc_process_find_with_channel(blc_process *processes, int processes_nb, blc_channel *channel);
//int blc_process_find_all_with_channel(blc_process ***found_processes, blc_process *processes, int processes_nb, blc_channel *channel);

int get_channel_index_by_name_or_id(char const *name);

void load_file(const char* filename, void*);
void unlink_unused();

void blc_update_shared_files(blc_process **processes, int *processes_nb, blc_channel *channels, int channels_nb);

#endif
