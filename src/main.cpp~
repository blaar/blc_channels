#include "blc.h"
#include <fcntl.h> // O_... constants
#include <sys/stat.h> // O_... constants

#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h> //tolower


/**
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */

blc_channel **channels_addresses;
struct blc_channel_info *channels_infos=NULL;
int channels_info_nb=0, channels_nb=0;

int sem_locked(sem_t *sem)
{
    if( sem==NULL) EXIT_ON_ERROR("sem_locked");
    if (sem_trywait(sem)==0) //Success to lock (it was not lock)
    {
        SYSTEM_ERROR_CHECK(sem_post(sem), -1, "");
        return 0;
    }
    else
    {
        if (errno != EAGAIN) EXIT_ON_SYSTEM_ERROR("sem_trywait");
        return 1;
    }
}

void display_channels()
{
    char sem_status[4];
    int i;
    struct blc_channel *channel;
    
    printf_underline('-', "%6s | sem | %10s  | type | %-32s | %-32s ", "id", "size" , "parameter", "name");
    FOR(i, channels_nb)
    {
        channel = channels_addresses[i];
        strcpy(sem_status, channel->sem_abp);
        if ((sem_status[0]=='a') && (sem_locked(channel->sem_ack))) sem_status[0]='A';
        if ((sem_status[1]=='b') && (sem_locked(channel->sem_block))) sem_status[1]='B';
        if ((sem_status[2]=='p') && (sem_locked(channel->sem_prot))) sem_status[2]='P';
        printf("%6d | %3s | %10ldb | %.4s | %-32s | %-32s\n", channel->id, sem_status, channel->size, UINT32_TO_STRING(channel->type), channel->parameter, channel->name);
    
    }
    printf("\n");
}

blc_channel *get_channel_by_name_or_id(char const *name)
{
    int i, id;
    char *end;
    
    if (name[0]=='/')
    {
        FOR(i, channels_nb) if (strcmp(channels_addresses[i]->name, name)) return channels_addresses[i];
    }
    else
    {
        id = strtol(name, &end, 10);
        if (end!=name && end[0]==0)
        {
            FOR(i, channels_nb) if (channels_addresses[i]->id == id) return channels_addresses[i];
        }else color_printf(BLC_RED, "You have to give either an id or a channel name stating by '/' instead of '%s'.\n", name);
    }
    return NULL;
}


void command_callback(char const *argument, void *pointer)
{
    int i;
    char tmp_buffer[NAME_MAX];
    size_t size;
    char *end, action = *(char*)pointer;
    blc_channel *channel;
    char tmp_sem_abp[4], sem_abp[4]="---";
    
    switch (action)
    {
        case 'a':case 'b':case 'p':case 'A':case 'B':case 'P':
            channel = get_channel_by_name_or_id(argument);
            if (channel)
            {
                if (strchr(channel->sem_abp, tolower(action)) == NULL) printf("The channel '%s' does not have '%c' semaphore.", argument,  tolower(action));
                else switch (action)
                {
                    case 'a':SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack), -1, "");break;
                    case 'A':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_ack), -1, "");break;
                    case 'b':SYSTEM_ERROR_CHECK(sem_post(channel->sem_block), -1, "");break;
                    case 'B':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_block), -1, "");break;
                    case 'p':SYSTEM_ERROR_CHECK(sem_post(channel->sem_prot), -1, "");break;
                    case 'P':SYSTEM_ERROR_CHECK(sem_wait(channel->sem_prot), -1, "");break;
                }
            }
            else printf("unknown blc_channel '%s'", argument);
            break;
        case 'c':
            if (argument[0] != '/') printf("The channel name must start with '/'");
            do
            {
                printf("Size of blc channel '%s' ('c' to cancel)?", argument);
                fgets(tmp_buffer, NAME_MAX, stdin);
                size = strtol(tmp_buffer, &end, 10);
                if (end != tmp_buffer)
                {
                    printf("abp (ack, block, protect) ?");
                    scanf("%3[apb]", tmp_sem_abp);
                    if (strchr(tmp_sem_abp, 'a')) sem_abp[0]='a';
                    if (strchr(tmp_sem_abp, 'b')) sem_abp[1]='b';
                    if (strchr(tmp_sem_abp, 'p')) sem_abp[2]='p';
                    
                    channel = new blc_channel(argument, size, sem_abp);
                    APPEND_ITEM(&channels_addresses, &channels_nb, &channel);
                    break;
                }
            }
            while(tmp_buffer[0] != 'c');
            break;
        case 'l':display_channels();
            break;
        case 'q':exit(0);
            break;
        case 'u':
            channel = get_channel_by_name_or_id(argument);
            if (channel == NULL)
            {
                if(argument[0]=='/')
                {
                    do
                    {
                        color_printf(BLC_RED, "'%s' is not a known channel.\nTry to unlink the shared memory file anyway (y/n).\n", argument);
                        fgets(tmp_buffer, NAME_MAX, stdin);
                        
                        if (strcmp(tmp_buffer, "y\n")==0)
                        {
                            if (shm_unlink(argument) == -1)
                            {
                                if (errno == ENOENT) printf("The shared memory '%s' does not exist.\n", argument);
                                else printf("Unlinking '%s'.\nSystem error:%s", argument, strerror(errno));
                            }
                            printf("\nSuccess unlinking shared memory '%s'\n\n", argument);
                            break;
                        }
                    }while(strcmp(tmp_buffer, "n\n") != 0);
                }
            }
            else
            {
                FOR(i, channels_nb) if (channels_addresses[i] == channel) break;
                channels_addresses[i]->unlink();
                channels_nb--;
                channels_addresses[i] = channels_addresses[channels_nb];
                MANY_REALLOCATIONS(&channels_addresses, channels_nb);
            }
            break;
    }
}

int channel_get_all()
{
    blc_channel *channel;
    int i, channels_nb = 0;
    struct blc_channel_info *info;
    sem_t *tmp_sem;
    char answer[NAME_MAX+1];
    char tmp_name[NAME_MAX+1];
    
    
    FOR(i, channels_info_nb)
    {
        info = &channels_infos[i];
        
        if (info->referenced_size == 0) PRINT_WARNING("Channel '%s' is of size 0. It has not been opened.", info->name);
        else if (strchr(info->sem_abp, 'p'))
        {
            tmp_sem=sem_open(info->name, O_RDWR, S_IRWXU);
            if (tmp_sem==SEM_FAILED)
            {
                if (errno==EACCES)
                {
                    color_printf(BLC_RED, "You do not have the right to open the protect semaphore '%s'.\n", info->name);
                }
                else if (errno!=ENOENT) EXIT_ON_SYSTEM_ERROR("Opening the protect semaphore '%s'", info->name);
                color_printf(BLC_RED,  "The protect semaphore '%s' does not exist", info->name);
                do
                {
                    printf("Do you want to create it ? (y/n)");
                    fgets(answer, NAME_MAX, stdin);
                    if (strcmp(answer, "y\n")==0)
                    {
                        SYSTEM_ERROR_CHECK(tmp_sem=sem_open(info->name, O_CREAT | O_EXCL), SEM_FAILED, "Channel '%s'", info->name);
                        SYSTEM_ERROR_CHECK(sem_close(tmp_sem), -1, "");
                    }
                }while(strcmp(answer, "n\n")!=0);
            }
            else sem_close(tmp_sem);
            
            channel = new blc_channel(info->name, info->referenced_size, info->sem_abp, info->type, info->parameter);
            APPEND_ITEM(&channels_addresses, &channels_nb, &channel);
        }
    }
    return channels_nb;
}

    
    int main(int argc, char **argv)
    {
        char const *interactive_mode=NULL, *help=NULL;
        char const *start_filter_name = "";
        
        terminal_ansi_detect();
        program_option_add(&interactive_mode, 'h', "help", 0, "Display this help");
        program_option_add(&interactive_mode, 'i', "interactive", 0, "Active the intercative mode");
        program_option_interpret_and_print_title(&argc, &argv);
        
        if (help) program_option_display_help();
        
        if (argc == 2) start_filter_name = argv[1];
        if (argc > 2) EXIT_ON_ERROR("You can have only one argument, which is the optional start name filter. You have %d arguments", argc-1);
        
        blc_command_add("c", command_callback, "name", "create a new blc_channel", (void*)"c");
        blc_command_add("l", command_callback, NULL, "list the blc_channels", (void*)"l");
        blc_command_add("u", command_callback, "channel", "unlink a channel", (void*)"u");
        blc_command_add("q", command_callback, NULL, "quit", (void*)"q");
        blc_command_add("a", command_callback, "channel", "send ack (sem_post)", (void*)"a");
        blc_command_add("A", command_callback, "channel", "wait for ack (sem_wait)", (void*)"A");
        blc_command_add("b", command_callback, "channel", "unblock the channel (sem_post)", (void*)"b");
        blc_command_add("B", command_callback, "channel", "block a channel (sem_wait)", (void*)"B");
        blc_command_add("p", command_callback, "channel", "unprotect a channel (sem_post)", (void*)"p");
        blc_command_add("P", command_callback, "channel", "protect a channel (sem_wait)", (void*)"P");
        
        channels_info_nb=blc_channel_get_all_infos(&channels_infos, start_filter_name);
        
        channels_nb=channel_get_all();
        
        //  channels_nb = blc_channel_get_all_infod(&channels_addresses, start_filter_name);
        
        if (interactive_mode)
        {
            channel_get_all();
            
            while (1)
            {
                display_channels();
                blc_command_display_help();
                blc_command_interpret();
                printf("\n");
            }
        }
        else display_channels();
        
        return 0;
    }
    
