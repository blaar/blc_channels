#include "blc_tools.h"
#include "blc_text.h"
#include "blc_program.h"
#include "blc_command.h"
#include "blc_realtime.h"

#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <sys/stat.h> // mode S_... constants
#include <sys/mman.h>
#include <sys/time.h> //gettiemofday
#include <pthread.h>

#include "core.hpp"

pthread_mutex_t display_mutex=PTHREAD_MUTEX_INITIALIZER;

blc_process **displayed_processes;
int displayed_processes_nb;

blc_file_opening *process_find_shared_memory(blc_process *process, char const *name){
    blc_file_opening **file_openning_pt;
    blc_file *file;
    
    FOR_EACH(file_openning_pt, process->openings_pt, process->openings_nb){
        file=file_openning_pt[0]->file;
#ifdef __OpenBSD__
        if ((strcmp(file->type, "PSXSHM")==0) && (strcmp(file->path, name)==0)){
            return *file_openning_pt;
        }
#else
        if ((strcmp(file->type, "REG")==0)  && strcmp(file->path, "/dev/shm") && (strcmp(file->path+strlen("/dev/shm"), name)==0)){ // ON Linux PSXSHM is in fact REG and the name is preceded by /dev/shm
            return *file_openning_pt;
        }
#endif

    }
    return NULL;
}

void unlink_unused(){
	blc_channel *channel;
	blc_file_opening *opening;
	blc_process *process;
    int i, j, channel_has_process;
    
    FREE(processes);
    processes_nb=0;

    // we get the list of processes using channels
    blc_update_shared_files(&processes, &processes_nb, channels, channels_nb);
    BLC_PTHREAD_CHECK(pthread_mutex_lock(&display_mutex), NULL);

    //For all channel we check if one of the process has this channel
    FOR(i, channels_nb){
    	channel_has_process=0;
        FOR(j, processes_nb){
            opening = process_find_shared_memory(&processes[j], channels[i].name);
            if (opening) {
            	channel_has_process=1;
            	break;
            }
        }
        if (channel_has_process==0){
              channels[i].remove();
              color_fprintf(BLC_GREEN, stderr, "remove '%s'\n", channels[i].name);
              REMOVE_ITEM_POSITION(&channels, &channels_nb, i);
                   	i--;
                   }
    }
    BLC_PTHREAD_CHECK(pthread_mutex_unlock(&display_mutex), NULL);
}


void view_processes(char const *name, void *)
{
    int index;
    blc_process *processes = NULL;
    blc_channel *channel;
    
    fprintf(stderr, "\nprocess details:\n");
    
    index = get_channel_index_by_name_or_id(name);
    
    FOR_EACH(channel, channels, channels_nb)
    /*   if (strcmp(file->channel->name, channels[index].name) == 0) break;
     if (file != files - 1)*/
    {
        fprintf(stderr, "%s:\n", channels[index].name);
     /*   FOR_EACH_INV(opening, file->openings, file->openings_nb)
        {
            APPEND_ITEM(&processes, &processes_nb, opening->process);
        }
        blc_processes_refresh(processes, processes_nb);
        FOR_EACH_INV(opening, file->openings, file->openings_nb){
            fprintf(stderr, "pid:%d %%cpu:%.2f mem:%d %%mem:%.2f %s\n", opening->process->pid, opening->process->cpu_percent, opening->process->mem, opening->process->mem_percent, opening->process->full_command_line);
        }*/
    }
    FREE(processes);
}
/*
void terminate_processes( char const *name, void*){
    int index, i;
    blc_process **processes_pt=NULL;
    int processes_pt_nb;
    
    index = get_channel_index_by_name_or_id(name);
    
    if (index==-1) color_fprintf(BLC_RED, stderr, "Channel unknown :'%s'\n", name);
    else{
        processes_pt_nb=blc_process_find_all_with_channel(&processes_pt, processes, processes_nb, &channels[index]);
        if (processes_nb==0) color_fprintf(BLC_RED, stderr, "no process to stop for channel '%s'\n", channels[index].name);
        FOR_INV(i, processes_pt_nb){
            SYSTEM_SUCCESS_CHECK(kill(processes_pt[i]->pid, SIGTERM), 0, NULL);
            color_fprintf(BLC_GREEN, stderr, "Terminating %d: '%s'\n", processes_pt[i]->pid, processes_pt[i]->command);
        }
    }
}*/
/*
void display_process(blc_process *process, int pos);


void display_output_channel(blc_channel *channel, int pos){
    blc_process *process;
    
    pos+=fprintf(stderr, "- %s \n  %*s\n    %*s", channel->name, pos, "|", pos, "->");
    
    FOR_EACH_INV(process, processes, processes_nb){
        if (GET_ITEM_POSITION( process->input_channels, process->input_channels_nb, &channel)!=-1){
            if (GET_ITEM_POSITION(displayed_processes, displayed_processes_nb, &process)==-1){
                fprintf(stderr, "[%s:%d]",  process->command, process->pid);
                APPEND_ITEM(&displayed_processes, &displayed_processes_nb, &process);
            }
        }
    }
}

void display_input_channel(blc_channel *channel, int pos){
    blc_process *process;
    int found=0;
    
    FOR_EACH_INV(process, processes, processes_nb){
        if (GET_ITEM_POSITION(process->output_channels, process->output_channels_nb, &channel)!=-1){
            display_process(process, pos);
            found=1;
        }
    }
    if (found==0) {
        fprintf(stderr, "- %s ->", channel->name);
    }
    
}

void display_process(blc_process *process, int pos){
    blc_channel **channel_pt;
    
    if (process->input_channels_nb==0){
        if (GET_ITEM_POSITION(displayed_processes, displayed_processes_nb, &process)==-1){
            pos = fprintf(stderr, "[%s:%d]", process->command, process->pid);
            APPEND_ITEM(&displayed_processes, &displayed_processes_nb, &process);
            FOR_EACH_INV(channel_pt, process->output_channels, process->output_channels_nb)
            display_output_channel(*channel_pt, pos);
        }
    }
    else { FOR_EACH_INV(channel_pt, process->input_channels, process->input_channels_nb) {
        display_input_channel(*channel_pt, pos);
    }
        //    display_process(process, pos);
    }
}*/
/*s
void display_dot_format(char const *name, void*){
    blc_process *process;
    (void)name;
    
    FREE(displayed_processes);
    displayed_processes_nb=0;
    
    fprintf(stderr, "\ngraph:\n\n");
    FOR_EACH_INV(process, processes, processes_nb) display_process(process, 0);
    fprintf(stderr, "\n\n");
    
}*/

void display_content( char const *name, void*){
    blc_channel *channel;
    int index, dim, length, i, j;
    uint32_t type_str;
    blc_dim *tmp_dims;
    
    pthread_mutex_lock(&display_mutex);
    
    index = get_channel_index_by_name_or_id(name);
    
    if (index == -1) color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist\n", name);
    else
    {
        channel = &channels[index];
        
        dim = 0;
        j = 0;
        
        tmp_dims = MANY_ALLOCATIONS(channel->dims_nb, blc_dim);
        memcpy(tmp_dims, channel->dims, channel->dims_nb * sizeof(blc_dim));
        
        channel->open(channels[index].name, O_RDONLY);
        
        fprintf(stderr, "Display %s :\n", channel->name);
        //  fprintf(stderr, "reading:%d\n", channel->control->blocking_readers_nb);
        while (dim != channel->dims_nb)
        {
            if (dim == 0)
            {
                length = tmp_dims[0].length;
                switch (channel->type)
                {
                    case 'TEXT': fprintf(stderr, "%-*s ", length, channel->chars);
                        break;
                    case 'INT8': FOR(i, length) fprintf(stderr, "%4d ", channel->chars[i + j]);
                        break;
                    case 'UIN8':
                        FOR(i, length)
                        fprintf(stderr, "%3u ", channel->uchars[i + j]);
                        break;
                    case 'IN16':
                        FOR(i, length)
                        fprintf(stderr, "%6d ", channel->ints16[i + j]);
                        break;
                    case 'UI16':
                        FOR(i, length)
                        fprintf(stderr, "%5u ", channel->uints16[i + j]);
                        break;
                    case 'IN32':
                        FOR(i, length)
                        fprintf(stderr, "%d ", channel->uints32[i + j]);
                        break;
                    case 'UI32':
                        FOR(i, length)
                        fprintf(stderr, "%u ", channel->uints32[i + j]);
                        break;
                    case 'FL32':
                        FOR(i, length)
                        fprintf(stderr, "%f ", channel->floats[i + j]);
                        break;
                    case 'FL64':
                        FOR(i, length)
                        fprintf(stderr, "%lf ", channel->doubles[i + j]);
                        break;
                    default:
                        EXIT_ON_ERROR("You cannot display type: '%.4s'", UINT32_TO_STRING(type_str, channel->type));
                }
                dim++;
            }
            else
            {
                if (dim==channel->dims_nb-1 ) fprintf(stderr, "\n");
                else fprintf(stderr, " ");
                
                tmp_dims[dim].length--;
                if (tmp_dims[dim].length == 0)
                {
                    tmp_dims[dim].length = channel->dims[dim].length;
                    dim++;
                }
                else dim--;
            }
        }
        FREE(tmp_dims);
        channel->~blc_channel();
    }
    pthread_mutex_unlock(&display_mutex);
    fprintf(stderr, "\n");

    
}
/*
void free_lock( char const *name, void*){
    blc_channel *channel;
    int index, error;
    
    index = get_channel_index_by_name_or_id(name);
    if (index == -1) color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist\n", name);
    channel = &channels[index];
    PTHREAD_CHECK(pthread_mutex_unlock(&channel->control->mutex), error, NULL);
}*/

void remove_channel( char const *name, void*)
{
    char buffer[NAME_MAX + 1];
    int index;
    
    index = get_channel_index_by_name_or_id(name);
    if (index == -1){
        if (name[0] == '/'){
            do{
                color_fprintf(BLC_RED, stderr, "'%s' is not a known channel.\nTry to unlink the shared memory file anyway (y/n).\n", name);
                SYSTEM_ERROR_CHECK(fgets(buffer, NAME_MAX, stdin), NULL, "(It may just be stdin that has been closed)");
                
                if (strcmp(buffer, "y\n") == 0){
                    if (shm_unlink(name) == -1){
                        if (errno == ENOENT) fprintf(stderr, "The shared memory '%s' does not exist.\n", name);
                        else fprintf(stderr, "Unlinking '%s'.\nSystem error:%s", name, strerror(errno));
                    }
                    color_fprintf(BLC_GREEN, stderr, "\nSuccess unlinking shared memory '%s'\n\n", name);
                    break;
                }
            } while (strcmp(buffer, "n\n") != 0);
        }
    }
    else{
        channels[index].remove();
        color_fprintf(BLC_GREEN, stderr, "\nChannel '%s' removed.\n", channels[index].name);
        REMOVE_ITEM_POSITION(&channels, &channels_nb, index);
    }
}

/*
void unlock_sem_var(char const *name, void*){
    int index;
    
    index = get_channel_index_by_name_or_id(name);
    if (index == -1) color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist\n", name);
    else {
        
        SYSTEM_ERROR_CHECK(sem_post(channels[index].sem_var), -1, "error id %%d", errno);
    }
}
*/
/*
 void graph(struct blc_file *file)
 {
 int j;
 struct process *process;
 
 FOR_INV(j, file->openings_nb){
 process = file->openings[j].process;
 fprintf(stderr, "[%d:%s, %c] ", process->pid, process->command, file->openings[j].access);
 }
 fprintf(stderr, "\n");
 }
 
 void tree_graph(struct file *file)
 {
 int j;
 struct process *process;
 struct file_opening *opening;
 
 
 FOR_EACH_INV(opening, file->openings, file->openings_nb){
 process = opening->process;
 switch (opening->access){
 case 'r': ;break;
 case 'w':while(found_nb=find_files_with_pid_and_access(process->pid, 'w'))
 {
 
 }
 break;
 process=blc_find_process(opening->process->pid);break;
 default: EXIT_ON_ERROR("Access '%c' not managed",opening->access );
 }
 }
 fprintf(stderr, "\n");
 }*/




void display_channels(char const *start_filter_name, void*){
    int width;
    blc_channel *channel;
    blc_process *process;
    uint32_t type_str, format_str;
    int has_process;
    blc_file_opening *opening;
    
    pthread_mutex_lock(&display_mutex);
    
    channels_nb = blc_channel_get_all_infos_with_filter(&channels, start_filter_name);
    
    FREE(processes);
    processes_nb=0;
    
    // we get the list of processes using channels
    blc_update_shared_files(&processes, &processes_nb, channels, channels_nb);
    
    underline_printf('-', "%6s | .-> ->^ | type |format|%-32s | %-32s ", "id", "dims", "name");
    FOR_EACH(channel,  channels, channels_nb){
 
        channel->open_semaphores();
        fprintf(stderr,   "%6d | %2d  %2d  | %.4s | %.4s |", channel->id, blc_sem_available(channel->sem_new_data), blc_sem_available(channel->sem_ack_data), UINT32_TO_STRING(type_str, channel->type), UINT32_TO_STRING(format_str, channel->format));
        
        SYSTEM_ERROR_CHECK(sem_close(channel->sem_new_data), -1, NULL);
        SYSTEM_ERROR_CHECK(sem_close(channel->sem_ack_data), -1, NULL);
        
        width = channel->fprint_dims(stderr);
        fprintf(stderr, "%*c", 32 - width, ' ');
        fprintf(stderr, " | %-32s\n",  channel->name);
        
        has_process=0;
    	//We look for process having channel->name opened
        FOR_EACH_INV(process, processes, processes_nb){
            opening = process_find_shared_memory(process, channel->name);

            if (opening){
                switch (opening->access){
                    case 'u': color_fprintf(BLC_RED, stderr, "<-");
                        break;
                    case 'r': color_fprintf(BLC_GREEN, stderr, "->");
                        break;
                    default: EXIT_ON_ERROR("Access mode '%c' not managed", opening->access );
                }
                fprintf(stderr, "[%s:%d] ", process->command, process->pid);
                has_process++;
            }
        }
        if (has_process) printf("\n");
        
    }
    fprintf(stderr, "\n");
    
//    display_dot_format(NULL, NULL);
    
    pthread_mutex_unlock(&display_mutex);
}

void create_channel(char const *argument, void*){
    blc_channel tmp_channel;
    blc_channel *channel;
    char tmp_buffer[NAME_MAX+1];
    char *end;
    int cancel=0, fd;
    size_t size;
    
    if (argument[0] != '/') printf("The channel name must start with '/'");
    else do{
        if (blc_channel_get_info_with_name(&tmp_channel, argument) != -1){
            color_printf(BLC_RED, "The blc_channel '%s' already exist.", tmp_channel.name);
            printf("\nDo you want to recreate it ? (y/n)");
            do{
                SYSTEM_ERROR_CHECK(fgets(tmp_buffer, NAME_MAX, stdin), NULL, "(It may just be stdin that has been closed)");
                if (strcmp(tmp_buffer, "y\n") == 0){
                    remove_channel( argument, NULL);
                    break;
                }
                else cancel = 1;
            } while (strcmp(tmp_buffer, "n\n") != 0);
        }
        
        fd = shm_open(argument, O_RDWR, S_IRWXU);
        if (fd != -1){
            color_printf(BLC_RED, "A shared memory of name '%s' already exists.\nDo you want to remove it before creating a new blc_channel ? (y/n)", argument);
            do{
                SYSTEM_ERROR_CHECK(fgets(tmp_buffer, NAME_MAX, stdin), NULL, "(It may just be stdin that has been closed)");
                if (strcmp(tmp_buffer, "y\n") == 0){
                    shm_unlink(argument);
                    break;
                }
                cancel = 1;
            } while (strcmp(tmp_buffer, "n\n") != 0);
        }
        if (cancel) break;
        
        printf("Size of blc channel '%s' ('c' to cancel)?", argument);
        SYSTEM_ERROR_CHECK(fgets(tmp_buffer, NAME_MAX, stdin), NULL, "(It may just be stdin that has been closed)");
        size = strtol(tmp_buffer, &end, 10);
        if (end != tmp_buffer){
            channel=APPEND_ALLOCATION(&channels, &channels_nb, blc_channel);
            channel->create(argument, BLC_CHANNEL_READ,  'UIN8', 'NDEF', 1, size);
            channel->~blc_channel();
            break;
        }
    } while (tmp_buffer[0] != 'c');
}

void graph_gnuplot_cb(char const *argument, void *user_data){
    int index, stdin_pipe[2], stdout_pipe[2], stderr_pipe[2];
    
    index = get_channel_index_by_name_or_id(argument);

    if (index!=-1){
        //We copy std pipes to simulate normal usage
        
        SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(stderr_pipe), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(stdin_pipe), -1, NULL);
        if (fork()==0){
            blc_command_ask_quit();

            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stderr_pipe[1], STDERR_FILENO), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdin_pipe[0] , STDIN_FILENO ), -1, EINTR, NULL);
            
            blc_close_pipe(stdout_pipe);
            blc_close_pipe(stderr_pipe);
            blc_close_pipe(stdin_pipe );
            
            execlp("o_gnuplot", "o_gnuplot", channels[index].name, NULL);
            exit(0);
        }
    }
    else color_eprintf(BLC_YELLOW, "Unknown blc_channel '%s'", argument);
}

void o_gtk_image_cb(char const *argument, void *user_data){
    int index, stdin_pipe[2], stdout_pipe[2], stderr_pipe[2];
    
    index = get_channel_index_by_name_or_id(argument);
    
    if (index!=-1){
        //We copy std pipes to simulate normal usage
        
        SYSTEM_ERROR_CHECK(pipe(stdout_pipe), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(stderr_pipe), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(stdin_pipe), -1, NULL);
        if (fork()==0){
            blc_command_ask_quit();
            
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdout_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stderr_pipe[1], STDERR_FILENO), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(stdin_pipe[0] , STDIN_FILENO ), -1, EINTR, NULL);
            
            blc_close_pipe(stdout_pipe);
            blc_close_pipe(stderr_pipe);
            blc_close_pipe(stdin_pipe );
            
            execlp("o_gtk_image", "o_gtk_image", channels[index].name, NULL);
            exit(0);
        }
    }
    else color_eprintf(BLC_YELLOW, "Unknown blc_channel '%s'", argument);
}

void control_cb(char const *argument, void *user_data){
    blc_channel *channel;
    int index;
    
    index = get_channel_index_by_name_or_id(argument);

    if (index != -1){
        channel = &channels[index];
        channel->open_semaphores();
        switch (*(char*)user_data)
        {
            case '.':
                SYSTEM_ERROR_CHECK(sem_post(channel->sem_new_data), -1, NULL);
                color_fprintf(BLC_GREEN, stderr, "new data event has been sent to '%s'\n", channel->name);
                break;
            case '^':
                SYSTEM_ERROR_CHECK(sem_post(channel->sem_ack_data), -1, NULL);
                color_fprintf(BLC_GREEN, stderr, "ack event has been sent to '%s'\nw", channel->name);
                break;
            default: EXIT_ON_ERROR("Argument unknown");
        }
        SYSTEM_ERROR_CHECK(sem_close(channel->sem_new_data), -1, NULL);
        SYSTEM_ERROR_CHECK(sem_close(channel->sem_ack_data), -1, NULL);
    }
}

void load_file_cb(const char* filename, void*){
    FILE *file;
    blc_channel tmp_channel;
    
    SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Loading channel '%s'", filename);
    tmp_channel.fscan_info(file, 1);
    tmp_channel.open(NULL, BLC_CHANNEL_WRITE);
    SYSTEM_SUCCESS_CHECK(fread(tmp_channel.data, tmp_channel.size, 1, file), 1, "Loading '%s'", tmp_channel.name);
    fclose(file);
    APPEND_ITEM(&channels, &channels_nb, &tmp_channel);
}

void save_channel_cb(const char* channel_name, void*){
    blc_channel *channel;
    char filename[PATH_MAX+1];
    FILE *file;
    int index;
    
    index=get_channel_index_by_name_or_id(channel_name);
    if (index==-1) EXIT_ON_ERROR("Channel '%s' does not exist.", channel_name);
    channel=&channels[index];
    
    do{
        fprintf(stderr, "filename: ");
        SYSTEM_ERROR_CHECK(scanf("%"STRINGIFY_CONTENT(PATH_MAX)"s", filename), -1, NULL);
    }while(access(filename, F_OK)==0);
    channel->open(channel_name, BLC_CHANNEL_READ);
    
    //  SYSTEM_ERROR_CHECK(file=fopen(filename, "w"), NULL, "Creating channel '%s'", channel->name);
    file=fopen(filename, "w");
    channel->fprint_info(file, 1);
    SYSTEM_SUCCESS_CHECK(fwrite(channel->data, channel->size, 1, file), 1, "Saving '%s'", channel->name);
    fclose(file);
}

void main_interactive_mode(const char *start_filter_name){
    blc_command_add("c", create_channel, "name", "create a new blc_channel", NULL);
    blc_command_add("d",  display_content, "channel", "display the content", NULL);
    blc_command_add("g",  graph_gnuplot_cb, "channel", "graph the blc_channel ( you need o_gnuplot installed)", NULL);
    blc_command_add("i",  o_gtk_image_cb, "channel", "display the image ( you need o_gtk_image installed)", NULL);


 //   blc_command_add("dot", display_dot_format, "channel", "display the content", NULL);
 //   blc_command_add("f", free_lock, "channel", "force to unlock mutex", NULL);
   // blc_command_add("l", display_channels, NULL, "refresh list of channels", NULL);
    blc_command_add("load", load_file_cb, "filename", "load a file", NULL);
    blc_command_add("save", save_channel_cb, "channel", "save a channel", NULL);
    blc_command_add("u", remove_channel , "channel", "unlink a channel", NULL);
    blc_command_add("U", (type_blc_command_cb)unlink_unused, NULL, "unlink unused channels", NULL);
    //blc_command_add("t", terminate_processes, "channel", "terminate (send SIGTERM) associated processes", NULL);
//    blc_command_add("v", view_processes, "channel", "view channel", NULL);
//    blc_command_add("V", unlock_sem_var , "channel", "unlock semvar", NULL);
    blc_command_add(".", control_cb, "channel", "send new data event", (void*)".");
    blc_command_add("^", control_cb, "channel", "send ack data event", (void*)"^");
    
    display_channels(start_filter_name, NULL);
    
    blc_channel_check_for_event((void(*)(void*))display_channels, (void*)start_filter_name);
    
    BLC_COMMAND_LOOP(-1){
        display_channels(start_filter_name, NULL);
        blc_command_display_help();
    }
}

int main(int argc, char **argv){
    char const  *unlink_name, *unlink_unused_flag, *force, *uncontrol_id, *start_filter_name;

    blc_channel channel_info;
    
    blc_program_set_description("Analyses and manages blc channels");
    blc_program_add_option(&unlink_name, 'u', "unlink", "blc_channel", "unlink the channel", NULL);
    blc_program_add_option(&uncontrol_id, 'C', "uncontrol", "id", "unlink the semaphores", NULL);
    blc_program_add_option(&unlink_unused_flag, 'U', "unlink_unused", NULL, "unlink unused channels", NULL);
    blc_program_add_option(&force, 'f', "force", NULL, "force the action", NULL);

    blc_program_add_parameter(&start_filter_name, "name_filter", 0, "filter the beginning of the blc_channel name", NULL);
    blc_program_init(&argc, &argv, NULL);
    
    if (unlink_name || unlink_unused_flag || uncontrol_id)
    {
        if (unlink_name)
        {
            if (blc_channel_get_info_with_name(&channel_info, unlink_name) != -1){
                channel_info.remove();
            }
            else{
                color_fprintf(BLC_RED, stderr, "Channel '%s' does not exist.\n", unlink_name);
                if (force){
                    fprintf(stderr, "We try to unlink the shared memory\n");
                    if (shm_unlink(unlink_name) == -1) color_fprintf(BLC_RED, stderr, "Impossible to remove shared memory '%s'.\n%s\n", unlink_name, strerror(errno));
                    else color_fprintf(BLC_GREEN, stderr, "'%s' has been removed.", unlink_name);
                }
                else fprintf(stderr, "You can try to remove the shared memory anyway using the --force option.\n");
            }
        }
        
        if (unlink_unused_flag) {
            channels_nb = blc_channel_get_all_infos_with_filter(&channels, start_filter_name);
            unlink_unused();
            FREE(channels);
            channels_nb=0;
        }
    }
    else {
        atexit(blc_quit);
        main_interactive_mode(start_filter_name);
    }
    return 0;
}

